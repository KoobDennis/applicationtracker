import json

class Activities:
    def __init__(self) -> None:
        self.activities = list()

    def load(self):
        with open('activities.json', 'r') as f:
            try:
                data = json.load(f)
                self.fromJson(data)
            except Exception:
                self.activities = list()

    def addTimeentry(self, activitynname, timeentry):
        activity = self._activity_exists(activitynname)

        if activity:
            activity.addTimeEntry(timeentry)
        else:
            activity = Activity(activitynname)
            activity.addTimeEntry(timeentry)
            self.activities.append(activity)

    def _activity_exists(self, activity):
        if not self.activities: 
            return False 

        for a in self.activities:
            if a.name == activity:
                return a

        return False

    def addActivity(self, name):
        self.activities.append(Activity(name))

    def toJson(self):
        return {
            "activities": self.activitiesToJson()
        }    
    
    def activitiesToJson(self):
        _activities = []
        for activity in self.activities:
            _activities.append(activity.toJson())
        
        return _activities

    def fromJson(self, data):
        for a in data["activities"]:
            for te in a["timeEntries"]:
                self.addTimeentry(a["name"], TimeEntry(te["start"], te["end"]))

class Activity:
    def __init__(self, name) -> None:
        self.name = name
        self.timeentries = []

    def addTimeEntry(self, entry):
        self.timeentries.append(entry)
    
    def toJson(self):
        return {
            "name": self.name,
            "timeEntries" : self.timeEntriesToJson()
        }
    
    def timeEntriesToJson(self):
        _timeentries = []
        for entries in self.timeentries:
            _timeentries.append(entries.toJson())
        
        return _timeentries

class TimeEntry:
    def __init__(self, starttime, endtime) -> None:
        self.startTime = starttime
        self.endTime = endtime
    
    def toJson(self):
        timedif = self.endTime - self.startTime
        return {
            "start": self.startTime,
            "end": self.endTime,
            "seconds": timedif 
        }