import win32gui
import time
import re
from activity import *

last_window_name = ""
activities = Activities()
activities.load()
starttime = time.time()
first_run = True

try:
    while True:
        current_window = win32gui.GetForegroundWindow()
        current_window_name = win32gui.GetWindowText(current_window)
        splitted_window_name = re.split('-|–| - | – |  ', current_window_name)
        
        if(splitted_window_name[-1] == ""): continue

        if first_run:            
            last_window_name = splitted_window_name[-1]
            first_run = False

        if splitted_window_name[-1] != last_window_name:
            endtime = time.time()
            timeentry = TimeEntry(starttime, endtime)
            activities.addTimeentry(last_window_name, timeentry)
            
            last_window_name = splitted_window_name[-1]
            starttime = time.time()
            
        with open('activities.json', 'w') as f:
            json.dump(activities.toJson(), f, indent=4, sort_keys=True)

        time.sleep(1)
except KeyboardInterrupt:
    print('interrupted!')